#include <iostream>
#include <fstream>
#include <string>
#include "../inc/Image.hpp"

using namespace std;

Image::Image(string imageFile){


	ppmOriginal = "doc/" + imageFile + ".ppm";
	read.open(ppmOriginal.c_str());

	while(!read.is_open()){
		cout << "File not found. Enter .ppm valid image: ";
		cin >> ppmOriginal;
		ppmOriginal = "doc/" + ppmOriginal + ".ppm";
		read.open(ppmOriginal.c_str());
	}


	readHeader();

	while (magicNumber != "P6" && magicNumber != "P3"){

		cout << "Incorrect format file. Enter .ppm valid iamge: ";
		cin >> ppmOriginal;
		ppmOriginal = "doc/" + ppmOriginal + ".ppm";
		read.open(ppmOriginal.c_str());

		while(!read.is_open()){
			cout << "File not found. Enter .ppm valid image: ";
			cin >> ppmOriginal;
			ppmOriginal = "doc/" + ppmOriginal + ".ppm";
			read.open(ppmOriginal.c_str());
		}

		readHeader();
	}

	readPixel();


}

void Image::readPixel(){

	char pix;

	for(int i = 0; i < width; i++){
		for (int j = 0; j < height; j++){
			read.get(pix);
						pixel[i][j][1] = (int) pix;
			read.get(pix);
                        pixel[i][j][2] = (int) pix;
			read.get(pix);
                        pixel[i][j][3] = (int) pix;
		}
	}

}

//readHeader will get header ppm image information
void Image::readHeader(){

	string header;
	ifstream aux(ppmOriginal.c_str());

	int info = 0;

	getline(read, header);
	while(header != "255"){
		if (header[0] != '#'){

			if(info == 0) aux >> magicNumber;
			if(info == 1) {
				aux >> height;
				aux >> width;}

			info++;
		}
	getline(aux, header);
	getline(read, header);
	}

	 maxColor = 255;

}


string Image::writePixel(string imageCopy){

	ppmCopy = "doc/" + imageCopy + ".ppm";

	write.open(ppmCopy.c_str());

	write << magicNumber << endl;
	write << height << " " << width << endl;
	write << maxColor << endl;


	for(int i = 0; i < width; i++){
		for(int j = 0; j < height; j++){
			write << pixel[i][j][1];
			write << pixel[i][j][2];
			write << pixel[i][j][3];
		}
	}

	string message = "New file created!";

	return message;


}


Image::~Image(){

}

string Image::getMagicNumber(){
	return magicNumber;
}

void Image::setMagicNumber(string magicNumber){
	this -> magicNumber = magicNumber;
}

int Image::getHeight(){
	return height;
}

void Image::setHeight(int height){
	this -> height = height;
}

int Image::getWidth(){
	return width;
}

void Image::setWidth(int width){
	this -> width = width;
}

int Image::getMaxColor(){
	return maxColor;
}

void Image::setMaxColor(int maxColor){
	this -> maxColor = maxColor;
}
