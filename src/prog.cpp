#include "../inc/Image.hpp"
#include "../inc/FilterNegative.hpp"
#include "../inc/FilterPolarized.hpp"
#include "../inc/FilterBlackWhite.hpp"
#include "../inc/FilterAverage.hpp"

string imageFile, imageCopy;
Image* image;

int main(){

	void applyFilterNegative();
	void applyFilterPolarized();
	void applyFilterBlackWhite();
	void applyFilterAverage();

	int menu;

	cout << "-----------------------------------" << endl;
	cout << "| FILTER APPLYER --- EP1 (00 UnB) |" << endl;
	cout << "-----------------------------------" << endl;
	cout << "1* Your ppm file must be into /doc folder" << endl;
	cout << "2* Your new filtered image is going to be into /doc folder" << endl << endl;


	cout << "Enter .ppm image name: ";
	cin >> imageFile;

	image = new Image(imageFile);

	cout << endl;
	cout << "(1) Negative Filter  (2) Polarized Filter  (3) Black&White Filter  (4) Average Filter" << endl;
	cout << "Filter to use: ";
	cin >> menu;
	cout << endl;

	switch(menu){
		case 1:{
			cout << "New image name: ";
			cin >> imageCopy;
			applyFilterNegative();
			break;
		}

		case 2:{
			cout << "New image name: ";
			cin >> imageCopy;
			applyFilterPolarized();
			break;
		}

		case 3:{
			cout << "New image name: ";
			cin >> imageCopy;
			applyFilterBlackWhite();
			break;
		}

		case 4:{
			cout << "New image name: ";
			cin >> imageCopy;
			applyFilterAverage();
			break;
		}

		default:
			cout << "Invalid value" << endl;
			break;
	}

	delete (image);
	return 0;
}


void applyFilterNegative(){


	FilterNegative fn;

	fn.applyFilter(*image);

	cout << image->writePixel(imageCopy) << endl;


}

void applyFilterPolarized(){

	FilterPolarized fp;

	fp.applyFilter(*image);

	cout << image->writePixel(imageCopy) << endl;


}

void applyFilterBlackWhite(){

	FilterBlackWhite fbw;

	fbw.applyFilter(*image);

	cout << image->writePixel(imageCopy) << endl;
}


void applyFilterAverage(){

	int filterSize;
	cout << endl;
	cout << "(3) 3x3  (5) 5x5  (7) 7x7" << endl;
	cout << "Filter size: ";
	cin >> filterSize;
	cout << endl;

	FilterAverage fa;

	switch(filterSize){

		case 3:{

		fa.applyFilter(*image, filterSize);
		cout << image->writePixel(imageCopy) << endl;
		break;

		}

		case 5:{
		
		fa.applyFilter(*image, filterSize);
		cout << image->writePixel(imageCopy) << endl;
		break;

		}

		case 7:{
		
		fa.applyFilter(*image, filterSize);
		cout << image->writePixel(imageCopy) << endl;
		break;

		}

		default:
			
			cout << "Incorret value!";
			applyFilterAverage();
			break;

	}

}