#include "../inc/FilterNegative.hpp"
#include <iostream>

FilterNegative::FilterNegative(){}

FilterNegative::~FilterNegative(){}

void FilterNegative::applyFilter(Image &img){

	Image* image = &img;

        for(int i = 0; i < image->getWidth(); i++){
                for(int j = 0; j < image->getHeight(); j++){
                        image->pixel[i][j][1] = image->getMaxColor() - image->pixel[i][j][1];
                        image->pixel[i][j][2] = image->getMaxColor() - image->pixel[i][j][2];
                        image->pixel[i][j][3] = image->getMaxColor() - image->pixel[i][j][3];
                }
        }

}
