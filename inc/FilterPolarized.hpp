#ifndef FILTERPOLARIZED_HPP
#define FILTERPOLARIZED_HPP

#include "Filter.hpp"

class FilterPolarized : public Filter{

public:
	FilterPolarized();
	~FilterPolarized();

	void applyFilter(Image &image);
};
#endif