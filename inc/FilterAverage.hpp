#ifndef FILTERAVERAGE_HPP
#define FILTERAVERAGE_HPP

#include "Filter.hpp"

class FilterAverage : public Filter{

public:
	FilterAverage();
	~FilterAverage();

	void applyFilter(Image &img, int filterSize);

};
#endif