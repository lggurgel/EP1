#ifndef FILTERNEGATIVE_HPP
#define FILTERNEGATIVE_HPP

#include "Filter.hpp"

class FilterNegative : public Filter{

public:
	FilterNegative();
	~FilterNegative();

	void applyFilter(Image &image);



};
#endif
