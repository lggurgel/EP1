#ifndef FILTER_HPP
#define FILTER_HPP

#include "Image.hpp"

using namespace std;

class Filter{

public:
	Filter();
	~Filter();
	
	void applyFilter(Image &image);



};
#endif
