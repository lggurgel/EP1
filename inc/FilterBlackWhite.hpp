#ifndef FILTERBLACKWHITE_HPP
#define FILTERBLACKWHITE_HPP

#include "Filter.hpp"

class FilterBlackWhite : public Filter{

public:
	FilterBlackWhite();
	~FilterBlackWhite();

	void applyFilter(Image &image);


};
#endif