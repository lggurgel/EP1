#ifndef IMAGE_HPP
#define IMAGE_HPP

#include <iostream>
#include <fstream>
#include <string>

using namespace std;

class Image{

private:
	string magicNumber;
	int height, width, maxColor;
	
	string ppmOriginal, ppmCopy;

	ifstream read;
	ofstream write;

	void readHeader();
	void readPixel();

public:
	Image(string imageFile);
	~Image();

		unsigned char pixel[1000][1000][4];

	string writePixel(string copy);

	string getMagicNumber();
	void setMagicNumber(string magicNumber);

	int getHeight();
	void setHeight(int height);

	int getWidth();
	void setWidth(int width);

	int getMaxColor();
	void setMaxColor(int maxColor);
};
#endif
