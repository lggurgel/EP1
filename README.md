# Orientação a Objetos 1/2016

## EP1 - C++

* 1 - Abra a imagem no formato PPM
* 2 - Leia o conteúdo da imagem
* 3 - Selecione o filtro a aplicar
* 4 - Salve o conteúdo em outro arquivo


### Como Compilar e Executar

Para compilar e executar o programa em um sistema operacional Linux, siga as seguintes instruções:

* Abra o terminal;
* Encontre o diretório raiz do projeto;
* Limpe os arquivos objeto:
	**$ make clean** 
* Compile o programa: 
	**$ make**
* Execute:
	**$ make run**

#### Observações:

* As imagens que serão processadas pelo programa devem ser inseridas na pasta '/doc'.
* A imagem resultado será encaminhada também para a pasta '/doc'.
